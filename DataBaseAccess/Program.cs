﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost serviceHost = new ServiceHost(typeof(DataTransfer));
            serviceHost.AddServiceEndpoint(typeof(IDataTransfer), new NetTcpBinding(), "net.tcp://localhost:15001/IDataTransfer");

            serviceHost.Open();
            Console.WriteLine("DataBaseAccess is open.");
            Console.ReadLine();
            serviceHost.Close();
        }
    }
}
