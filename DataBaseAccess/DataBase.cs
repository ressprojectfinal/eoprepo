﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess
{
    public class DataBase
    {
        //static Dictionary<string, GeographicArea> geographicDictionaty = new Dictionary<string, GeographicArea>();
        public static List<ElectricityConsumption> XmlList;

        //Tuple predstavlja kljuc se sastoji od sifre geografskog podrucja (string) i tacnog vremena (iz datetime parametra i sata iz XML fajla)
        public static Dictionary<Tuple<string, DateTime>, ElectricityConsumption> EstimatedConsumption;
        public static Dictionary<Tuple<string, DateTime>, ElectricityConsumption> RealizedConsumption;
        //static Dictionary<string, ElectricityConsumption> electricityDictionary = new Dictionary<string, ElectricityConsumption>();
    }
}
