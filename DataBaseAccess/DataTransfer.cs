﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Xml;
using Common;

namespace DataBaseAccess
{
    public class DataTransfer : IDataTransfer
    {
        public ChannelFactory<IXMLReading> channel;
        public IXMLReading proxy;
        public List<ElectricityConsumption> GetFromDB(DateTime date, string area)
        {
            return TakeFromBase("Server=localhost;Database=Consumptions;Trusted_Connection=True;", date, area);
        }

        public string XMLToDataBase()
        {
            Console.WriteLine("Reading XML files...");
            channel = new ChannelFactory<IXMLReading>(new NetTcpBinding(), "net.tcp://localhost:15000/IXMLReading");
            proxy = channel.CreateChannel();
            List<string> filenames = proxy.GetFilenames();
            foreach (string file in filenames)
            {
                ProcessFilenames(file, filenames);
            }


            channel.Close();
            Console.WriteLine("Writing into DB finished");
            return "Succsessfully updated DateBase";
        }

        private void ProcessFilenames(string file, List<string> filenames)
        {
            DataTable dataAreas = new DataTable();
            dataAreas.Columns.Add("Id");
            dataAreas.Columns.Add("Area");
           // bool b = true;
            List<Tuple<int, string>> filesInDB = new List<Tuple<int, string>>();

            SqlConnection sqlConnection = new SqlConnection("Server=localhost;Database=Consumptions;Trusted_Connection=True;");
            SqlCommand sqlCmd = new SqlCommand("SELECT * FROM dbo.Filenames2", sqlConnection);  //provera da li nesto imamo u tabeli Areas,ako nemamo pisemo sifre geo podrucja
            sqlConnection.Open();

            SqlDataReader readerAreas = sqlCmd.ExecuteReader();

            if (readerAreas.HasRows)
            { 
                while (readerAreas.Read())
                {
                    filesInDB.Add(new Tuple<int, string>(readerAreas.GetInt32(0), readerAreas.GetString(1)));
                }

                List<bool> matches = new List<bool>(filesInDB.Count);
                for (int i = 0; i < matches.Count; i++)
                {
                    matches[i] = false;
                }
                int idNonMatch = 0;

                if (filesInDB.Count == filenames.Count)
                {
                    for (int i = 0; i < filenames.Count; i++)
                    {
                        for (int j = 0; j < filesInDB.Count; j++)
                        {
                            if (filenames[i] == filesInDB[j].Item2)
                            {
                                matches[j] = true;
                                idNonMatch = -1;
                            }
                        }
                        if (idNonMatch == 0)
                        {
                            idNonMatch = i;
                        }
                    }

                    //poredi values is filenames[idnonmatch] i mesta u matches gde je vrednost false


                }
                else
                {

                }
                
            }
            else
            {
                while (readerAreas.Read())
                {
                    dataAreas.Rows.Add(readerAreas.GetInt32(0), readerAreas.GetString(1)); //stavi iz baze u visual bazu da nam bude vidljivo

                }
            }
            sqlConnection.Close();


            /* SqlCommand sqlCmd2 = new SqlCommand("INSERT INTO dbo.Filenames2(Id,Filename) VALUES (@Id,@Filename)", sqlConnection);
             sqlCmd2.Parameters.AddWithValue("@Id", dataAreas.Rows.Count);
             sqlCmd2.Parameters.AddWithValue("@Filename", file);

             sqlConnection.Open();

             try
             {
                 var result = sqlCmd2.ExecuteNonQuery();
                 Console.WriteLine("Uspesno upisan");
             }
             catch (Exception ex)
             {
                 Console.WriteLine("Greska: " + ex.Message);
             }

             sqlConnection.Close();*/

            //radi se tek kad se provere svi uslovi i logika za file - da li treba da se cita??? da li je novi fajl?? ako jeste, radi ovo
            List<ElectricityConsumption> lista = proxy.SendXML(file);
            Console.WriteLine("Writing files into DataBase");
            WriteToDataBase(lista);


            /*if (b == false)
                {
                    channel = new ChannelFactory<IXMLReading>(new NetTcpBinding(), "net.tcp://localhost:15000/IXMLReading");
                    proxy = channel.CreateChannel();

                    //List<ElectricityConsumption> lista1 = proxy.XmlRead(filenames[i]);
                    //List<ElectricityConsumption> lista2 = proxy.XmlRead(file);
                    channel.Close();

                    

                    SqlCommand sqlCmd2 = new SqlCommand("INSERT INTO dbo.Filenames2(Id,Filename) VALUES (@Id,@Filename)", sqlConnection);
                    sqlCmd2.Parameters.AddWithValue("@Id", dataAreas.Rows.Count);
                    sqlCmd2.Parameters.AddWithValue("@Filename", file);

                    sqlConnection.Open();

                    try
                    {
                        var result = sqlCmd2.ExecuteNonQuery();
                        Console.WriteLine("Uspesno upisan");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Greska: " + ex.Message);
                    }

                    sqlConnection.Close();

                    //radi se tek kad se provere svi uslovi i logika za file - da li treba da se cita??? da li je novi fajl?? ako jeste, radi ovo
                    List<ElectricityConsumption> lista = proxy.SendXML(file);
                    Console.WriteLine("Writing files into DataBase");
                    WriteToDataBase(lista);
                }
                if (b == true)
                {
                    if (dataAreas.Rows.Count== 0)
                    {
                        SqlCommand sqlCmd2 = new SqlCommand("INSERT INTO dbo.Filenames2(Id,Filename) VALUES (@Id,@Filename)", sqlConnection);
                        sqlCmd2.Parameters.AddWithValue("@Id", dataAreas.Rows.Count);
                        sqlCmd2.Parameters.AddWithValue("@Filename", file);

                        sqlConnection.Open();

                        try
                        {
                            var result = sqlCmd2.ExecuteNonQuery();
                            Console.WriteLine("Uspesno upisan");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Greska: " + ex.Message);
                        }

                        sqlConnection.Close();

                        //radi se tek kad se provere svi uslovi i logika za file - da li treba da se cita??? da li je novi fajl?? ako jeste, radi ovo
                        List<ElectricityConsumption> lista = proxy.SendXML(file);
                        Console.WriteLine("Writing files into DataBase");
                        WriteToDataBase(lista);
                    }
                }
                else
                {
                    SqlCommand sqlCmd2 = new SqlCommand("INSERT INTO dbo.Filenames2(Id,Filename) VALUES (@Id,@Filename)", sqlConnection);
                    sqlCmd2.Parameters.AddWithValue("@Id", dataAreas.Rows.Count);
                    sqlCmd2.Parameters.AddWithValue("@Filename", file);

                    sqlConnection.Open();

                    try
                    {
                        var result = sqlCmd2.ExecuteNonQuery();
                        Console.WriteLine("Uspesno upisan");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Greska: " + ex.Message);
                    }

                    sqlConnection.Close();

                    //radi se tek kad se provere svi uslovi i logika za file - da li treba da se cita??? da li je novi fajl?? ako jeste, radi ovo
                    List<ElectricityConsumption> lista = proxy.SendXML(file);
                    Console.WriteLine("Writing files into DataBase");
                    WriteToDataBase(lista);
                }*/



            



        }

        public virtual void WriteToDataBase(List<ElectricityConsumption> l)
        {
            if (DataBase.XmlList == null)
                DataBase.XmlList = new List<ElectricityConsumption>(); //pravi novu listu ako je nema

            foreach (ElectricityConsumption e in l)
            {
                ProcessGeoArea(e.GeographicArea);
                DataBase.XmlList.Add(e);

                SqlConnection sqlConnection = new SqlConnection("Server=localhost;Database=Consumptions;Trusted_Connection=True;");

                SqlCommand sqlCmd = new SqlCommand("INSERT INTO dbo.Realised(Date,Area,Value,Type) VALUES (@Date,@Area,@Load,@Type)", sqlConnection);

                sqlCmd.Parameters.AddWithValue("@Date", e.Time);
                sqlCmd.Parameters.AddWithValue("@Area", e.GeographicArea);
                sqlCmd.Parameters.AddWithValue("@Load", e.Load);
                sqlCmd.Parameters.AddWithValue("@Type", e.TypeConsumption);

                sqlConnection.Open();

                try
                {
                    var result = sqlCmd.ExecuteNonQuery();
                    Console.WriteLine("Uspesno upisan");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Greska: " + ex.Message);
                }

                sqlConnection.Close();
            }
        }

        private void ProcessGeoArea(string geographicArea)
        {
            DataTable dataAreas = new DataTable();
            dataAreas.Columns.Add("Id");
            dataAreas.Columns.Add("Area");
            bool b = false;

            SqlConnection sqlConnection = new SqlConnection("Server=localhost;Database=Consumptions;Trusted_Connection=True;");

            SqlCommand sqlCmd = new SqlCommand("SELECT * FROM dbo.Areas", sqlConnection);  //provera da li nesto imamo u tabeli Areas,ako nemamo pisemo sifre geo podrucja

            sqlConnection.Open();

            SqlDataReader readerAreas = sqlCmd.ExecuteReader();

            if (readerAreas.HasRows)
            {
                while (readerAreas.Read())
                {
                    dataAreas.Rows.Add(readerAreas.GetInt32(0), readerAreas.GetString(1)); //stavi iz baze u visual bazu da nam bude vidljivo

                    foreach (DataRow dr in dataAreas.Rows)
                    {
                        if (geographicArea == readerAreas.GetString(1))
                        {
                            b = true;//ako se bar jedna vrednost podudara znaci da imamo u tabeli i ne trebamo dodavati novo podrucje
                        }
                    }
                }

            }
            sqlConnection.Close();


            if (b == false)
            {
                SqlCommand sqlCmd2 = new SqlCommand("INSERT INTO dbo.Areas(Id,Area) VALUES (@Id,@Area)", sqlConnection);
                sqlCmd2.Parameters.AddWithValue("@Id", dataAreas.Rows.Count);
                sqlCmd2.Parameters.AddWithValue("@Area", geographicArea);
                sqlConnection.Open();

                try
                {
                    var result = sqlCmd2.ExecuteNonQuery();
                    Console.WriteLine("Uspesno upisan");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Greska: " + ex.Message);
                }

                sqlConnection.Close();
            }

        }

        public virtual List<ElectricityConsumption> TakeFromBase(string conn, DateTime dt, string area)
        {
            SqlConnection sqlConnection = new SqlConnection(conn);
            List<ElectricityConsumption> lista = new List<ElectricityConsumption>();

            DataTable data = new DataTable();
            data.Columns.Add("Date");
            data.Columns.Add("Area");
            data.Columns.Add("Load");
            data.Columns.Add("Type");
            ElectricityConsumption el = new ElectricityConsumption();

            SqlCommand sqlCmd3 = new SqlCommand("SELECT * FROM dbo.Realised WHERE (DATEPART(yy,Date) = @Year AND DATEPART(MM,Date) = @Month AND DATEPART(DD,Date) = @Day) AND Area = @Area", sqlConnection);

            sqlCmd3.Parameters.AddWithValue("@Year", dt.Year);
            sqlCmd3.Parameters.AddWithValue("@Month", dt.Month);
            sqlCmd3.Parameters.AddWithValue("@Day", dt.Day);
            sqlCmd3.Parameters.AddWithValue("@Area", area);

            sqlConnection.Open();

            SqlDataReader reader = sqlCmd3.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Console.WriteLine("{0}\t{1}\t{2}", reader.GetDateTime(0), reader.GetString(1), reader.GetInt32(2), reader.GetString(3));
                    data.Rows.Add(reader.GetDateTime(0), reader.GetString(1), reader.GetInt32(2), reader.GetString(3));


                }
                foreach (DataRow dr in data.Rows)
                {
                    ElectricityConsumption ec = new ElectricityConsumption() { Time = Convert.ToDateTime(dr[0]), GeographicArea = dr[1].ToString(), Load = Convert.ToInt32(dr[2]), TypeConsumption = dr[3].ToString() };
                    lista.Add(ec);
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }
            //reader.Close();
            sqlConnection.Close();

            foreach (ElectricityConsumption ele in lista)
            {
                Console.WriteLine(ele.Time);
                Console.WriteLine(ele.Load);
                Console.WriteLine(ele.GeographicArea);
                Console.WriteLine(ele.TypeConsumption);
            }

            return lista;
        }

        public List<string> GetAreas() //uraditi test za ovo
        {
            SqlConnection sqlConnection = new SqlConnection("Server=localhost;Database=Consumptions;Trusted_Connection=True;");
            List<string> l = new List<string>();

            DataTable dataAreas = new DataTable();
            dataAreas.Columns.Add("Id");
            dataAreas.Columns.Add("Area");


            SqlCommand sqlCmd = new SqlCommand("SELECT * FROM dbo.Areas", sqlConnection);

            sqlConnection.Open();

            SqlDataReader readerAreas = sqlCmd.ExecuteReader();

            if (readerAreas.HasRows)
            {
                while (readerAreas.Read())
                {
                    dataAreas.Rows.Add(readerAreas.GetInt32(0), readerAreas.GetString(1));
                }

                foreach (DataRow dr in dataAreas.Rows)
                {
                    string s = Convert.ToString(dr[1]);
                    l.Add(s);

                }

            }

            sqlConnection.Close();

            return l;
        }
    }
}