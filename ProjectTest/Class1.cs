﻿using Common;
using DataBaseAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using XMLReader;

namespace ProjectTest

{
    [TestClass]
    public class Class1
    {
        /*[TestMethod]
        public void LoadDbWithMock() //mok u bazu
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            ElectricityConsumption e = new ElectricityConsumption();
            e.GeographicArea = "VOJ";
            e.Load = 0;
            e.Time = new DateTime(1, 1, 1, 1, 1, 1);
            e.TypeConsumption = "ostv";
            list.Add(e);

            Mock<XMLReading> moqloader = new Mock<XMLReading>("Server=localhost;Database=Consumptions;Trusted_Connection=True;", "C:/Users/lu/Desktop/Projecta/ProjectTest/bin/Debug/fajlovi");
            moqloader.Setup(obj => obj.ReadAllFiles()).Returns(list);

            MoqueTestLoader testLoader = new MoqueTestLoader(moqloader.Object);
            testLoader.DoThing();
        }*/

       /* [TestMethod]
        public void XMLReadMockTest() //citanje xml fajlova(mok)
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            List<ElectricityConsumption> NovaLista = new List<ElectricityConsumption>();
            ElectricityConsumption e = new ElectricityConsumption();
            e.GeographicArea = "VOJ";
            e.Load = 0;
            e.Time = new DateTime(1, 1, 1, 1, 1, 1);
            e.TypeConsumption = "ostv";
            list.Add(e);

            Mock<XMLReading> moqloader = new Mock<XMLReading>();
            moqloader.Setup(obj => obj.ReadAllFiles()).Returns(list);

            MoqueTestLoader testLoader = new MoqueTestLoader(moqloader.Object);
            NovaLista = testLoader.DajPodatke();

            Assert.IsTrue(NovaLista != null);

        }*/

        [TestMethod]
        public void DbReadMockTest()//citanje iz mokovane baze
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            List<ElectricityConsumption> NewList = new List<ElectricityConsumption>();
            ElectricityConsumption e = new ElectricityConsumption();
            e.GeographicArea = "VOJ";
            e.Load = 0;
            e.Time = new DateTime(1, 1, 1, 1, 1, 1);
            e.TypeConsumption = "ostv";
            list.Add(e);

            Mock<DataTransfer> moqloader = new Mock<DataTransfer>();
            moqloader.Setup(obj => obj.TakeFromBase("Server=localhost;Database=CopyConsumptions;Trusted_Connection=True;", new DateTime(2018, 5, 7), "VOJ")).Returns(list);

            MoqueTestLoader testLoader = new MoqueTestLoader(moqloader.Object);
            NewList = testLoader.DeliverDataFromBase();

            Assert.IsTrue(NewList != null);
        }

        [TestMethod]
        public void GetFileNamesTest()//citanje mokovanih fajlova
        {
            List<string> list = new List<string>();
            List<string> NewList = new List<string>();
            list.Add("ostv_2018_05_07.xml");
            Mock<XMLReading> moqloader = new Mock<XMLReading>();
            moqloader.Setup(obj => obj.GetFilenames()).Returns(list);

            MoqueTestLoader testLoader = new MoqueTestLoader(moqloader.Object);
            NewList = testLoader.GetFNames();
            Assert.IsTrue(NewList != null);
        }


        [TestMethod]
        public void ReadingOneXMLTest()//Citanje iz jednog (mock) XML fajla
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            List<ElectricityConsumption> NewList = new List<ElectricityConsumption>();
            ElectricityConsumption e = new ElectricityConsumption();
            e.GeographicArea = "VOJ";
            e.Load = 0;
            e.Time = new DateTime(1, 1, 1, 1, 1, 1);
            e.TypeConsumption = "ostv";
            list.Add(e);

            Mock<XMLReading> moqloader = new Mock<XMLReading>();
            moqloader.Setup(obj => obj.XmlRead(@"C:\Users\lu\Desktop\faks\REES\RES_Projekat\Project\ProjectTest\bin\Debug\fajlovi\ostv_2018_05_07.xml")).Returns(list);

            MoqueTestLoader testLoader = new MoqueTestLoader(moqloader.Object);
            NewList = testLoader.DeliverOneXMLFile();

            Assert.IsTrue(NewList != null);
        }

        [TestMethod]
        public void XMLReadTest()//Citanje iz mokovanog xml-a???
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            List<ElectricityConsumption> NewList = new List<ElectricityConsumption>();
            ElectricityConsumption e = new ElectricityConsumption();
            e.GeographicArea = "VOJ";
            e.Load = 0;
            e.Time = new DateTime(1, 1, 1, 1, 1, 1);
            e.TypeConsumption = "ostv";
            list.Add(e);

            Mock<XMLReading> moqloader = new Mock<XMLReading>();
            moqloader.Setup(obj => obj.SendXML(@"C:\Users\lu\Desktop\faks\REES\RES_Projekat\Project\ProjectTest\bin\Debug\fajlovi\ostv_2018_05_07.xml")).Returns(list);

            MoqueTestLoader testLoader = new MoqueTestLoader(moqloader.Object);
            NewList = testLoader.DeliverXMLFiles();

            Assert.IsTrue(NewList != null);
        }

        [TestMethod]
        public void WriteToDateBaseTest()///******
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            ElectricityConsumption e = new ElectricityConsumption();
            e.GeographicArea = "VOJ";
            e.Load = 0;
            e.Time = new DateTime(1, 1, 1, 1, 1, 1);
            e.TypeConsumption = "ostv";
            list.Add(e);

            Mock<DataTransfer> moqloader = new Mock<DataTransfer>();
            moqloader.Setup(obj => obj.WriteToDataBase(list));

            MoqueTestLoader testLoader = new MoqueTestLoader(moqloader.Object);
        }
    }

    

    

    class MoqueTestLoader
    {
        public XMLReading XMLReadingTest;
        public DataTransfer dataTransferTest;
        public MoqueTestLoader(XMLReading xml)
        {
            XMLReadingTest = xml;
        }

        public MoqueTestLoader(DataTransfer dtt)
        {
            dataTransferTest = dtt;
        }

        /*public List<ElectricityConsumption> DajPodatke()
        {
            List<ElectricityConsumption> lista = new List<ElectricityConsumption>();
            lista = dbl.ReadAllFiles();
            return lista;

        }*/

        /*public void DoThing()
        {
            List<ElectricityConsumption> lista = new List<ElectricityConsumption>();
            lista=dbl.ReadAllFiles();
            //dtr.WriteToDataBase(lista);
        }*/

        public List<ElectricityConsumption> DeliverDataFromBase()
        {
            List<ElectricityConsumption> lista = new List<ElectricityConsumption>();
            lista = dataTransferTest.TakeFromBase("Server=localhost;Database=CopyConsumptions;Trusted_Connection=True;", new DateTime(2018, 5, 7), "VOJ");
            return lista;
        }

        public List<ElectricityConsumption> DeliverXMLFiles()
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            list = XMLReadingTest.SendXML(@"C:\Users\lu\Desktop\faks\REES\RES_Projekat\Project\ProjectTest\bin\Debug\fajlovi\ostv_2018_05_07.xml");
            return list;
        }

        public List<ElectricityConsumption> DeliverOneXMLFile()
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            list = XMLReadingTest.XmlRead(@"C:\Users\lu\Desktop\faks\REES\RES_Projekat\Project\ProjectTest\bin\Debug\fajlovi\ostv_2018_05_07.xml");
            return list;
        }

        public List<String> GetFNames()
        {
            List<string> fileNames = new List<string>();
            fileNames = XMLReadingTest.GetFilenames();
            return fileNames;
        }


    }
}
