﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [DataContract]
    public class GeographicArea
    {
        private string name;
        private string id;

        [DataMember]
        public string Name { get => name; set => name = value; }
        [DataMember]
        public string Id { get => id; set => id = value; }

    }
}