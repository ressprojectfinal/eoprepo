﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [DataContract]
    public class ElectricityConsumption
    {
        private string geographicArea;
        private DateTime time;
        private int load;
        private string typeConsumption;

        public ElectricityConsumption()
        {
            this.geographicArea = "";
            this.load = 0;
        }

        public ElectricityConsumption(string geographicArea, DateTime time, int load,string type)
        {
            DateTime dt = new DateTime(time.Year, time.Month, time.Day, time.Hour, 0, 0);
            this.geographicArea = geographicArea;
            this.time = dt;
            this.load = load;
            this.typeConsumption = type;
        }
        [DataMember]
        public string GeographicArea { get => geographicArea; set => geographicArea = value; }

        [DataMember]
        public DateTime Time { get => time; set => time = value; }

        [DataMember]
        public int Load { get => load; set => this.load = value; }

        [DataMember]
        public string TypeConsumption { get => typeConsumption; set => typeConsumption = value; }
    }
}