﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [ServiceContract]
    public interface IDataTransfer
    {
        [OperationContract]
        List<ElectricityConsumption> GetFromDB(DateTime date, string area);

        [OperationContract]
        string XMLToDataBase();

        [OperationContract]
        List<string> GetAreas();

    }
}
