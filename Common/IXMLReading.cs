﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [ServiceContract]
    public interface IXMLReading
    {
        [OperationContract]
        List<ElectricityConsumption> SendXML(string file);

        [OperationContract]
        List<string> GetFilenames();

        [OperationContract]
        List<ElectricityConsumption> XmlRead(string filename);

    }
}
