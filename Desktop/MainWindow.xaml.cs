﻿using Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static BindingList<Display> SourceList { get; set; }

        public ChannelFactory<IDataTransfer> channel;
        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();         
        }

        private void ImportData(object sender, RoutedEventArgs e)
        {
            channel = new ChannelFactory<IDataTransfer>(new NetTcpBinding(), "net.tcp://localhost:15001/IDataTransfer");
            IDataTransfer proxy = channel.CreateChannel();

            proxy.XMLToDataBase();

            channel.Close();
        }

        private void WriteData(object sender, RoutedEventArgs e)
        {
            IzabranaOstvarenaIspis.ItemsSource=ParseData(datePicker.SelectedDate.Value, Podrucja_Combobox.SelectedItem.ToString());
        }

        private void Evidention(object sender, RoutedEventArgs e)
        {
            channel = new ChannelFactory<IDataTransfer>(new NetTcpBinding(), "net.tcp://localhost:15001/IDataTransfer");
            IDataTransfer proxy = channel.CreateChannel();

            Podrucja_Combobox.ItemsSource = proxy.GetAreas();

            channel.Close();

        }

        private BindingList<Display> ParseData(DateTime date, string areaCode)
        {
            SourceList = new BindingList<Display>();

            List<Display> dispData = new List<Display>();
            channel = new ChannelFactory<IDataTransfer>(new NetTcpBinding(), "net.tcp://localhost:15001/IDataTransfer");
            IDataTransfer proxy = channel.CreateChannel();

            List<ElectricityConsumption> data = proxy.GetFromDB(date, areaCode);

            foreach (ElectricityConsumption e in data)
            {
                Display d = new Display();
                if(e.TypeConsumption == "ostv")
                {
                    d.Sat = e.Time.Hour;
                    d.Ostvarenap = e.Load;
                    for (int i = 0; i < data.Count; i++)
                    {
                        if (data[i].Time.Hour == d.Sat && data[i].GeographicArea == e.GeographicArea && data[i].TypeConsumption == "prog")
                        {
                            d.Prognoziranap = data[i].Load;
                            break;
                        }
                    }
                    d.Odstupanje = (Math.Abs(d.Ostvarenap - d.Prognoziranap)) / d.Ostvarenap * 100;
                    SourceList.Add(d);
                }
            }
            return SourceList;

        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}