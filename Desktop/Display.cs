﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop
{
    public class Display
    {
        int sat;
        int prognoziranap;
        int ostvarenap;
        float odstupanje;

        public Display(int sat, int prognoziranap, int ostvarenap, float odstupanje)
        {
            this.Sat = sat;
            this.Prognoziranap = prognoziranap;
            this.Ostvarenap = ostvarenap;
            this.Odstupanje = odstupanje;
        }

        public Display()
        {
            this.Sat = 0;
            this.Prognoziranap = 0;
            this.Ostvarenap = 0;
            this.Odstupanje = 0;
        }

        public int Sat { get => sat; set => sat = value; }
        public int Prognoziranap { get => prognoziranap; set => prognoziranap = value; }
        public int Ostvarenap { get => ostvarenap; set => ostvarenap = value; }
        public float Odstupanje { get => odstupanje; set => odstupanje = value; }
    }
}
