﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace XMLReader
{
    class Program
    {
        
        static void Main(string[] args)
        {
            ServiceHost serviceHost = new ServiceHost(typeof(XMLReading));
            serviceHost.AddServiceEndpoint(typeof(IXMLReading), new NetTcpBinding(), "net.tcp://localhost:15000/IXMLReading");

            serviceHost.Open();
            Console.WriteLine("XMLReader is open.");
            Console.ReadLine();
            serviceHost.Close();

        }
    }
}
