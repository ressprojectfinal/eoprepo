﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;
using Common;

namespace XMLReader
{
    public class XMLReading : IXMLReading
    {

        public virtual List<ElectricityConsumption> SendXML(string file)
        {
            List<ElectricityConsumption> list = new List<ElectricityConsumption>();
            list = XmlRead(file);
            return list;
        }

        public virtual List<ElectricityConsumption> XmlRead(string filename)
        {
            string pomTime = String.Empty;
            string pomLoad = String.Empty;

            ElectricityConsumption e = new ElectricityConsumption();
            List<ElectricityConsumption> listE = new List<ElectricityConsumption>();

            if (filename.Contains("ostv"))
            {
                e.TypeConsumption = "ostv";

            }
            else if (filename.Contains("prog"))
            {
                e.TypeConsumption = "prog";
            }
            else
            {
                Console.WriteLine("Name of file is not valid");
                return null;
            }


            XmlDocument doc = new XmlDocument();
            doc.Load(filename);
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/PROGNOZIRANI_LOAD/STAVKA");

            string area = String.Empty;
            int time;
            int load;

            foreach (XmlNode node in nodes)
            {
                pomTime = node.SelectSingleNode("SAT").InnerText;
                Int32.TryParse(pomTime, out time);
                pomLoad = node.SelectSingleNode("LOAD").InnerText;
                Int32.TryParse(pomLoad, out load);
                area = node.SelectSingleNode("OBLAST").InnerText;

                e.GeographicArea = area;
                e.Load = load;

                string[] str = filename.Split('_', '.');

                int year = Convert.ToInt32(str[2]);
                int month = Convert.ToInt32(str[3]);
                int day = Convert.ToInt32(str[4]);

                if (time == 24)
                {
                    time = 0;
                }

                e.Time = new DateTime(year, month, day, time, 0, 0);

                listE.Add(new ElectricityConsumption(e.GeographicArea, e.Time, e.Load, e.TypeConsumption));
               
            }
            return listE;
        }
        

        public virtual List<string> GetFilenames()
        {
            string[] fileNames = System.IO.Directory.GetFiles(@"C:\Users\lu\Desktop\faks\REES\RES_Projekat\Project\ProjectTest\bin\Debug\fajlovi");
            return fileNames.ToList();
        }

        
    }
}